package stevens.software.notetakingapp;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.Menu;
import com.google.android.material.navigation.NavigationView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stevens.software.notetakingapp.fragments.CategoryFragment;
import stevens.software.notetakingapp.fragments.NotesFragment;
import stevens.software.notetakingapp.fragments.addFragments.AddCategoryFragment;
import stevens.software.notetakingapp.fragments.addFragments.AddNoteFragment;
import stevens.software.notetakingapp.model.Category;
import stevens.software.notetakingapp.retrofitApiClient.ApiClient;
import stevens.software.notetakingapp.service.CategoryService;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CategoryService categoryService = ApiClient.getClient().create(CategoryService.class);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_notes, R.id.nav_categories)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        final SubMenu categoriesSubMenu = menu.addSubMenu(R.string.all_categories_submenu_title);

        Call<List<Category>> getCategories = categoryService.getCategories();
        getCategories.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    List<Category> responseCategories = response.body();
                    for(Category category : responseCategories){
                        categoriesSubMenu.add(12, 12, 1, category.getCategoryName());
                    }
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                    System.out.println("Re " + response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                System.out.println("Network Error :: " + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed(){
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_add_note:
                AddNoteFragment addNoteFragment = new AddNoteFragment();
                this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, addNoteFragment)
                        .addToBackStack(null)
                        .commit();
                item.setCheckable(true);
                toolbar.setTitle(item.getTitle());
                break;
            case R.id.nav_add_category:
                AddCategoryFragment addCategoryFragment = new AddCategoryFragment();
                this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, addCategoryFragment)
                        .addToBackStack(null)
                        .commit();
                item.setCheckable(true);
                toolbar.setTitle(item.getTitle());
                break;
            case R.id.nav_categories:
                CategoryFragment categoryFragment = new CategoryFragment();
                this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, categoryFragment)
                        .addToBackStack(null)
                        .commit();
                item.setCheckable(true);
                toolbar.setTitle(item.getTitle());
                break;
            case 12:
            case R.id.nav_notes:
                NotesFragment notesFragment = new NotesFragment();

                Bundle bundle = new Bundle();
                bundle.putString("Category name",  item.getTitle().toString());
                notesFragment.setArguments(bundle);

                this.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, notesFragment)
                        .addToBackStack(null)
                        .commit();
                item.setCheckable(true);
                if(item.getItemId() == 12){
                    toolbar.setTitle("All " + item.getTitle() + " Notes");
                }else{
                    toolbar.setTitle(item.getTitle());
                }
                break;
        }
        closeDrawer();
        return true;
    }

    public void closeDrawer(){
        drawer.closeDrawers();
    }


}
