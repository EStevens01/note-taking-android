package stevens.software.notetakingapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stevens.software.notetakingapp.fragments.addFragments.AddCategoryFragment;
import stevens.software.notetakingapp.retrofitApiClient.ApiClient;
import stevens.software.notetakingapp.model.Category;
import stevens.software.notetakingapp.recyclerViewAdapter.CategoryRecyclerViewAdapter;
import stevens.software.notetakingapp.service.CategoryService;
import stevens.software.notetakingapp.R;

public class CategoryFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notes_fragment, container, false);
        CategoryService categoryService = ApiClient.getClient().create(CategoryService.class);

        final Context context = getContext();
        final List<Category> categories = new ArrayList<>();
        TextView textView = view.findViewById(R.id.activityTitle);
        textView.setText(R.string.category_fragment_title);

        RecyclerView notesList = view.findViewById(R.id.notesList);
        final CategoryRecyclerViewAdapter categoryRecyclerViewAdapter = new CategoryRecyclerViewAdapter(context, categories);
        notesList.setAdapter(categoryRecyclerViewAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        notesList.setLayoutManager(layoutManager);

        FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCategoryFragment addCategoryFragment = new AddCategoryFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, addCategoryFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        Call<List<Category>> getCategories = categoryService.getCategories();
        getCategories.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    List<Category> responseNotes = response.body();
                    categories.addAll(responseNotes);
                    categoryRecyclerViewAdapter.setCategories(categories);
                    categoryRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                    System.out.println("Re " + response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                System.out.println("Network Error :: " + t.getLocalizedMessage());
            }
        });

        return view;
    }

}


