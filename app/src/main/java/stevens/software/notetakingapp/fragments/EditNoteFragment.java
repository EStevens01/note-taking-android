package stevens.software.notetakingapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stevens.software.notetakingapp.retrofitApiClient.ApiClient;
import stevens.software.notetakingapp.model.Note;
import stevens.software.notetakingapp.service.NoteService;
import stevens.software.notetakingapp.R;

public class EditNoteFragment extends Fragment {
    final NoteService noteService = ApiClient.getClient().create(NoteService.class);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_edit_note, container, false);

        //need to send the object rather than individual parts
        final Integer noteId = getArguments().getInt("Note ID");
        String title = getArguments().getString("Note Title");
        String description = getArguments().getString("Note Description");

        final TextView noteTitle = view.findViewById(R.id.editNoteTitle);
        final TextView noteDescription =  view.findViewById(R.id.note);
        noteTitle.setText(title);
        noteDescription.setText(description);

        Button deleteButton = view.findViewById(R.id.deleteNoteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View buttonView) {
                deleteNote(noteId);
                assert getFragmentManager() != null;
                if (getFragmentManager().getBackStackEntryCount() == 1) {
                    NotesFragment notesFragment = new NotesFragment();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, notesFragment)
                            .addToBackStack(null)
                            .commit();

                }else if (getFragmentManager().getBackStackEntryCount() > 0)  {
                    getFragmentManager().popBackStackImmediate();

                }

            }
            });

        final Button updateButton = view.findViewById(R.id.updateNoteButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View buttonView) {
                String newNoteTitle = noteTitle.getText().toString();
                String newNoteDescription = noteDescription.getText().toString();

                Note updatedNote = new Note();
                updatedNote.setNote(newNoteDescription);
                updatedNote.setTitle(newNoteTitle);

                Call<Note> updateNote = noteService.updateNote(noteId, updatedNote);
                updateNote.enqueue(new Callback<Note>() {
                    @Override
                    public void onResponse(Call<Note> call, Response<Note> response) {
                        assert getFragmentManager() != null;
                        if (getFragmentManager().getBackStackEntryCount() == 1) {
                            NotesFragment notesFragment = new NotesFragment();
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, notesFragment)
                                    .addToBackStack(null)
                                    .commit();

                        }else if (getFragmentManager().getBackStackEntryCount() > 0)  {
                            getFragmentManager().popBackStackImmediate();
                        }
                    }

                    @Override
                    public void onFailure(Call<Note> call, Throwable t) {
                    }
                });
            }
        });
        return view;
        }

    public void deleteNote(Integer noteId){
        Call<Note> deleteNote = noteService.deleteNote(noteId);
        deleteNote.enqueue(new Callback<Note>() {
            @Override
            public void onResponse(Call<Note> call, Response<Note> response) {

            }

            @Override
            public void onFailure(Call<Note> call, Throwable t) {
            }
        });
    }

}


