package stevens.software.notetakingapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stevens.software.notetakingapp.fragments.addFragments.AddNoteFragment;
import stevens.software.notetakingapp.retrofitApiClient.ApiClient;
import stevens.software.notetakingapp.model.Category;
import stevens.software.notetakingapp.service.CategoryService;
import stevens.software.notetakingapp.model.Note;
import stevens.software.notetakingapp.service.NoteService;
import stevens.software.notetakingapp.recyclerViewAdapter.NotesRecyclerViewAdapter;
import stevens.software.notetakingapp.R;

public class NotesFragment extends Fragment {
    private NoteService noteService;
    private CategoryService categoryService;
    private List<Note> notes;
    private NotesRecyclerViewAdapter noteListNotesRecyclerViewAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.notes_fragment, container, false);
        noteService = ApiClient.getClient().create(NoteService.class);
        categoryService = ApiClient.getClient().create(CategoryService.class);

        Context context = getContext();
        notes = new ArrayList<>();

        String pageTitle;
        if(getArguments() != null){
            pageTitle = getArguments().getString("Category name");
        }else{
            pageTitle = "All Notes";
        }

        TextView textView = view.findViewById(R.id.activityTitle);
        textView.setText(pageTitle);

        RecyclerView notesList = view.findViewById(R.id.notesList);
        noteListNotesRecyclerViewAdapter = new NotesRecyclerViewAdapter(context, notes);
        notesList.setAdapter(noteListNotesRecyclerViewAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        notesList.setLayoutManager(gridLayoutManager);

        FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNoteFragment addNoteFragment = new AddNoteFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, addNoteFragment, "findThisFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        //get notes based on a category
        if(!pageTitle.equals("All Notes")) {
            Call<Category> getCategoryByName = categoryService.getCategoryByName(pageTitle);
            getCategoryByName.enqueue(new Callback<Category>() {
                @Override
                public void onResponse(Call<Category> call, Response<Category> response) {
                    if (response.isSuccessful()) {
                        Category category = response.body();
                        getCategoriesNotes(category);
                    } else {
                        System.out.println("Request Error :: " + response.errorBody());
                        System.out.println("Re " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<Category> call, Throwable t) {
                    System.out.println("Network Error :: " + t.getLocalizedMessage());
                }
            });
        }else{
            //get all notes
            Call<List<Note>> getNotes = noteService.getNotes();
            getNotes.enqueue(new Callback<List<Note>>() {
                @Override
                public void onResponse(Call<List<Note>> call, Response<List<Note>> response) {
                    if (response.isSuccessful()) {
                        List<Note> responseNotes = response.body();
                        notes.addAll(responseNotes);
                        noteListNotesRecyclerViewAdapter.setNotes(notes);
                        noteListNotesRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        System.out.println("Request Error :: " + response.errorBody());
                        System.out.println("Re " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<List<Note>> call, Throwable t) {
                    System.out.println("Network Error :: " + t.getLocalizedMessage());
                }
            });
        }
        return view;
    }

    public void getCategoriesNotes(Category category){
        Call<List<Note>> getNotesFromCategory = noteService.getNotesFromCategoryId(category.getCategoryId());
        getNotesFromCategory.enqueue(new Callback<List<Note>>() {
            @Override
            public void onResponse(Call<List<Note>> call, Response<List<Note>> response) {
                if (response.isSuccessful()) {
                    List<Note> responseNotes = response.body();
                    notes.addAll(responseNotes);
                    noteListNotesRecyclerViewAdapter.setNotes(notes);
                    noteListNotesRecyclerViewAdapter.notifyDataSetChanged();
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                    System.out.println("Re " + response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Note>> call, Throwable t) {
                System.out.println("Network Error :: " + t.getLocalizedMessage());
            }
        });
    }
}
