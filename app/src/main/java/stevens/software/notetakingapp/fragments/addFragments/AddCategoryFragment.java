package stevens.software.notetakingapp.fragments.addFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stevens.software.notetakingapp.retrofitApiClient.ApiClient;
import stevens.software.notetakingapp.model.Category;
import stevens.software.notetakingapp.service.CategoryService;
import stevens.software.notetakingapp.MainActivity;
import stevens.software.notetakingapp.R;

public class AddCategoryFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_category, container, false);
        final CategoryService categoryService = ApiClient.getClient().create(CategoryService.class);

        Button saveButton = view.findViewById(R.id.saveCategoryButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View buttonView) {
                TextView category = view.findViewById(R.id.category);
                String categoryText = category.getText().toString();

                Category newCategory = new Category();
                newCategory.setCategoryName(categoryText);

                Call<Category> saveCategory = categoryService.saveCategory(newCategory);
                saveCategory.enqueue(new Callback<Category>() {
                    @Override
                    public void onResponse(Call<Category> call, Response<Category> response) {
                        startMainActivity();
                    }
                    @Override
                    public void onFailure(Call<Category> call, Throwable t) {
                    }
                });
            }
        });
        return view;
    }

    public void startMainActivity(){
        Intent activityIntent = new Intent(getContext(), MainActivity.class);
        startActivity(activityIntent);
    }
}
