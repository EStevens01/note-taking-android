package stevens.software.notetakingapp.fragments.addFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import stevens.software.notetakingapp.retrofitApiClient.ApiClient;
import stevens.software.notetakingapp.model.Category;
import stevens.software.notetakingapp.service.CategoryService;
import stevens.software.notetakingapp.MainActivity;
import stevens.software.notetakingapp.model.Note;
import stevens.software.notetakingapp.service.NoteService;
import stevens.software.notetakingapp.R;

public class AddNoteFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner spinner;
    private Integer selectedCategoryId;
    private List<Category> categories;
    private Map<Integer, Category> allCategories;
    private View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_add_note, container, false);
        final NoteService noteService = ApiClient.getClient().create(NoteService.class);
        final CategoryService categoryService = ApiClient.getClient().create(CategoryService.class);


        allCategories = new HashMap<>();
        categories = new ArrayList<>();
        spinner = view.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        Button saveButton = view.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View buttonView) {
                TextView noteTitle = view.findViewById(R.id.noteTitle);
                String noteTitleText = noteTitle.getText().toString();
                TextView noteDescription = view.findViewById(R.id.noteDescription);
                String noteDescriptionText = noteDescription.getText().toString();
                Category selectedCategory = allCategories.get(selectedCategoryId);

                Note newNote = new Note();
                newNote.setTitle(noteTitleText);
                newNote.setNote(noteDescriptionText);
                newNote.setCategory(selectedCategory);

                Call<Note> saveNote = noteService.saveNote(newNote);
                saveNote.enqueue(new Callback<Note>() {
                    @Override
                    public void onResponse(Call<Note> call, Response<Note> response) {
                        startMainActivity();
                    }

                    @Override
                    public void onFailure(Call<Note> call, Throwable t) {
                    }
                });
            }
        });

        Call<List<Category>> getCategories = categoryService.getCategories();
        getCategories.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    List<Category> responseNotes = response.body();
                    categories.addAll(responseNotes);

                    for(Category category : responseNotes){
                        allCategories.put(category.getCategoryId(), category);
                    }
                    populateSpinner(responseNotes);
                } else {
                    System.out.println("Request Error :: " + response.errorBody());
                    System.out.println("Re " + response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                System.out.println("Network Error :: " + t.getLocalizedMessage());
            }
        });
        return view;
        }

    public void startMainActivity(){
        Intent activityIntent = new Intent(getContext(), MainActivity.class);
        startActivity(activityIntent);
        }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedCategoryId = categories.get(position).getCategoryId();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void populateSpinner(List<Category> categories){
        ArrayAdapter<Category> dataAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }

}
