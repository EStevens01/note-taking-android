package stevens.software.notetakingapp.recyclerViewAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import stevens.software.notetakingapp.R;
import stevens.software.notetakingapp.model.Category;

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.CategoryViewHolder> {

    private List<Category> categoriesList;
    private LayoutInflater layoutInflater;

    public CategoryRecyclerViewAdapter(Context context, List<Category> myDataset) {
        categoriesList = myDataset;
        layoutInflater = LayoutInflater.from(context);

    }

    public void setCategories(List<Category> categories) {
        categoriesList = categories;
        notifyItemInserted(categoriesList.size());
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View noteItemView = layoutInflater.inflate(R.layout.note_list_item, parent, false);
        return new CategoryViewHolder(noteItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category note = categoriesList.get(position);
        holder.noteTitle.setText(note.getCategoryName());
        holder.noteDescription.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView noteTitle;
        TextView noteDescription;

        public CategoryViewHolder(View noteViewItem) {
            super(noteViewItem);
            noteTitle = noteViewItem.findViewById(R.id.noteTitle);
            noteDescription = noteViewItem.findViewById(R.id.noteDescription);
        }

    }
}

