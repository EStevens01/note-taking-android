package stevens.software.notetakingapp.recyclerViewAdapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import stevens.software.notetakingapp.R;
import stevens.software.notetakingapp.fragments.EditNoteFragment;
import stevens.software.notetakingapp.model.Note;

public class NotesRecyclerViewAdapter extends RecyclerView.Adapter<NotesRecyclerViewAdapter.NoteViewHolder> {
    private List<Note> noteList;
    private LayoutInflater layoutInflater;

    public NotesRecyclerViewAdapter(Context context, List<Note> myDataset) {
        noteList = myDataset;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setNotes(List<Note> notes) {
        noteList = notes;
        notifyItemInserted(noteList.size());
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View noteItemView = layoutInflater.inflate(R.layout.note_list_item, parent, false);

        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) noteItemView.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 4;
        noteItemView.setLayoutParams(lp);
        return new NoteViewHolder(noteItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final NoteViewHolder holder, int position) {
        Note note = noteList.get(position);

        final String title = note.getTitle();
        final String description = note.getNote();
        final Integer id = note.getNoteId();
        holder.noteTitle.setText(title);
        holder.noteDescription.setText(description);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                EditNoteFragment editNoteFragment = new EditNoteFragment();

                //need to send as an object
                Bundle bundle = new Bundle();
                bundle.putInt("Note ID", id);
                bundle.putString("Note Title", title);
                bundle.putString("Note Description", description);

                editNoteFragment.setArguments(bundle);
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, editNoteFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public static class NoteViewHolder extends RecyclerView.ViewHolder {
        TextView noteTitle;
        TextView noteDescription;

        public NoteViewHolder(View noteViewItem) {
            super(noteViewItem);
            noteTitle = noteViewItem.findViewById(R.id.noteTitle);
            noteDescription = noteViewItem.findViewById(R.id.noteDescription);
        }
    }
}


