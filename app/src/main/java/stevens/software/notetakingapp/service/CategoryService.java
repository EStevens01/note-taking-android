package stevens.software.notetakingapp.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import stevens.software.notetakingapp.model.Category;

public interface CategoryService {
    @GET("categories")
    Call<List<Category>> getCategories();

    @POST("/category")
    Call<Category> saveCategory(@Body Category category);

    @GET("/category/name/{categoryName}")
    Call<Category> getCategoryByName(@Path("categoryName") String name);
}
