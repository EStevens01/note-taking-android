package stevens.software.notetakingapp.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import stevens.software.notetakingapp.model.Note;

public interface NoteService {

    @GET("note/{id}")
    Call<Note> getNote(@Path("id") Integer id);

    @GET("notes")
    Call<List<Note>> getNotes();

    @POST("/note")
    Call<Note> saveNote(@Body Note note);

    @PUT("/note/{noteId}")
    Call<Note> updateNote(@Path("noteId") Integer id, @Body Note note);

    @DELETE("/note/{id}")
    Call<Note> deleteNote(@Path("id") Integer id);

    @GET("/notes/{category}")
    Call<List<Note>> getNotesFromCategoryId(@Path("category") Integer category);
}
